//Application Window Component Constructor
function ApplicationWindow() {
	//load component dependencies
	var FirstView = require('ui/common/FirstView');
	//var LogoView = require('ui/common/LogoView');
	var LogoView = require('ui/common/imgRotator');
	
	//create component instance
	var self = Ti.UI.createWindow({
		backgroundColor:'#3D3242',
		navBarHidden:true,
		exitOnClose:true
	});
		
	//construct UI
	var firstView = new FirstView();
	self.add(firstView);
	
	//Add Logo View to the current window
	var logoView = new LogoView();
	self.add(logoView);
	
	self.orientationModes = [Ti.UI.PORTRAIT];
	return self;
}

//make constructor function the public component interface
module.exports = ApplicationWindow;

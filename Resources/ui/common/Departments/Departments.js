//Departments Window

// create var for the currentWindow  
var currentWin = Ti.UI.currentWindow;

// set the data from the database to the array  
function setData() {
	
	//Database Connection
    var db = Ti.Database.install('../TAMUSA_APP.sqlite','DEPARTMENTS');  
    var rows = db.execute('SELECT DISTINCT DepartmentsID, DepartmentsName FROM DEPARTMENTS ORDER BY DepartmentsName ASC');
	
	//Loop through the records and set the Table title
	var dataArray = [];  
	while (rows.isValidRow())  
	{  
    	dataArray.push({title:'' + rows.fieldByName('DepartmentsName') + '', hasChild:true, path:'DepartmentMajors.js', ID:'' + rows.fieldByName('DepartmentsID') + ''});  //Next File Path
    	rows.next();  
	};
	
	//Attach array to tableView
	tableview.setData(dataArray); 
};
 
// create table view  
var tableview = Ti.UI.createTableView({  
});

tableview.addEventListener('click', function(e)  
{ 
    if (e.rowData.path)  
    {
        //Create Dynamic window to contain the details
        var win = Ti.UI.createWindow({
        	backgroundColor:'#000',
            url:e.rowData.path,  
            title:'Majors'  
        });  
        
        //Pass values to next page
        var departmentMajors = e.rowData.ID;  
        win.departmentMajors = departmentMajors;
        
        //Open the Window  
        win.open();  
    }  
});

// add the tableView to the current window  
currentWin.add(tableview);

// call the setData function to attach the database results to the array  
setData();  
function LogoView(){
	
	//load component dependencies
	//Ti.include('imgRotator.js');
	
	var borderTop = Ti.UI.createView({
    	//backgroundColor: '#702e3d',
    	height:2,
    	top:0
	});
	
	//create object instance, a parasitic subclass of Observable
	var self = Ti.UI.createView({
		//backgroundColor:'#702e3d',
		//backgroundColor:'#000',
		bottom:0,
		height:250,
		//borderColor:'#702e3d',
		//borderWidth:3
	});
	
	self.add(borderTop);
	
	//Create View for bottom Logo
	var logo = Ti.UI.createImageView({
		//image:"/images/logo.png",
		top:2,
		bottom:2
	});
	
	//Attach Image Rotator to Logo View
	//logo.add(viewRotator);
	
	//Attach Image View
	self.add(logo);

	return self;
}

module.exports = LogoView;
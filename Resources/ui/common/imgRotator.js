function imgRotator(){
	
	var borderTop = Ti.UI.createView({
    	//backgroundColor: '#702e3d',
    	height:2,
    	top:0
	});
	
	//create object instance, a parasitic subclass of Observable
	var self = Ti.UI.createView({
		//backgroundColor:'#702e3d',
		backgroundColor:'#000',
		bottom:0,
		height:250,
		//borderColor:'#702e3d',
		//borderWidth:3
	});
	
	self.add(borderTop);
	
	// Ads Container view
	var viewRotator = Titanium.UI.createImageView({
	    top:2,
		bottom:2
	    //zindex: 30
	 
	});
	
	// /Define and set slide variable to 0
	var slide = 0;
	
	// Loop through the Rotator Images folder
	for (var i=0; i < slide.length; i++) {
		// Define Ads Array
		slide[i] = {
			img: 'images/Rotator/' + i + '.png', 
			displayTime: 3000 // Set the time to display 3000 = 3sec
		}
	};
	
	//Count the slides
	var j = 0;
	var countSlides = slide.length;
			
	for (j; j < countSlides; j++) {
		// Get the next Slide
		var getNextSlide = function() {
			var slide = slide[j];
		}
					
		// Call the SlideShow function and set the views background image to the slide
		var showSlide = function(slide) {
			viewRotator.backgroundImage = slide.img; //Add the slides as a background image
			setTimeout(function() {
				showSlide(getNextSlide());
			}, slide.displayTime); // Calls the setTimeout function of javascript and sets its value to the property displayTime
		};
			
		// Show the slides
		showSlide(getNextSlide());
	};

// Add Rotator to main window
self.add(viewRotator);
	
return self;
	
	/////////////////////////////
	/*
		If Necessary, you can manually build the Array
		var slides = {};
		var slide[0] = {img: 'images/0.jpg', displayTime: 3000};
		var slide[1] = {img: 'images/1.jpg', displayTime: 3000};
		var slide[2] = {img: 'images/2.jpg', displayTime: 3000};
		var slide[3] = {img: 'images/3.png', displayTime: 3000};
		slides.push(slide[0]);
		slides.push(slide[1]);
		slides.push(slide[2]);
		slides.push(slide[3]);
		
		var getSlide = slide[0].img;
	*/
	/////////////////////////////
}

module.exports = imgRotator;